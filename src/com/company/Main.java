package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ArrayList;

public class Main extends JFrame implements ActionListener {

    private List<Imageable> imageables = new ArrayList<>();
    private ImageDao imageDao;

    public ImageDao getImageDao() {
        return imageDao;
    }

    public void setImageDao(ImageDao imageDao) {
        this.imageDao = imageDao;
        imageDao.generateExampleData();
    }

    public List<Imageable> getImageables() {
        return imageables;
    }

    public void setImageables(List<Imageable> imageables) {
        this.imageables = imageables;
    }

    private JPanel jPanel;

    private ImageGenerator simpleImageGenerator;

    public Main() {
        createFrameLayout();
        setImageDao(new ExampleImageData());
        simpleImageGenerator = new SimpleImageGenerator(getImageDao().findAll());
        try {
            imageables = simpleImageGenerator.generate();
        } catch (NoImageException e) {
            e.printStackTrace();
        }

        for (int i = 1; i <= 4; i++) {
            JButton jButton = new JButton("");
            ImageIcon imageIcon = new ImageIcon(imageables.get(i - 1).getFileName());
            jButton.setIcon(imageIcon);
            jButton.addActionListener(this);
            jButton.setName(imageables.get(i - 1).getClass().getSimpleName());
            // jButton.setName("" + i);
            jPanel.add(jButton);                //tworzenie buttonów
        }
        //dorzucenie do tego sztucznego kontenera JPanel przycisków
        add(jPanel);
    }

    private void createFrameLayout() {
        setSize(500, 500);       //ustala wielkość okna
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);     // sprawia, że będzie działał exit
        setVisible(true);        //widoczne okno

        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));       //ustawienie jLabel nad obrazkami
        JLabel jLabel = new JLabel("Co nie jest kotem");
        jLabel.setFont(new Font("Serif", Font.PLAIN, 24));
        add(jLabel);


        jPanel = new JPanel();       // odpowiednik div
        jPanel.setLayout(new GridLayout(2, 2));

    }


    public static void main(String[] args) {
        // write your code here
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Main();     //tworzenie okna
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Kliknieto mnie!");
        JButton clickedButton = (JButton) e.getSource();
        int theNumberWithSameClass = 0;
        for (Imageable imageable : getImageables()) {
            if (imageable.getClass().getSimpleName().equals(clickedButton.getName())) {
                theNumberWithSameClass++;
            }
        }

        System.out.println("jestem " + clickedButton.getName());

        if (theNumberWithSameClass == 1) {
            JOptionPane.showMessageDialog(this, "Gratulacje");

        } else {
            JOptionPane.showMessageDialog(this, "Kiepsko");

        }
        System.out.println(clickedButton.getClass().toString());

    }


}
