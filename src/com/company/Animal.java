package com.company;

public class Animal implements Imageable{
    private String filename;
    public Animal(String filename) {
        this.filename = filename;
    }

    @Override
    public String getFileName() {
        return filename;
    }
}
